<!doctype html>
<html lang="en">

  <head>
    <meta charset="utf-8">

    <title>
      Revisiting tabular material models for computational mechanics  
    </title> 

    <meta name="description" content="@IIT, Bombay, July 2022">
    <meta name="author" content="Biswajit Banerjee">

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <!-- My colors -->
    <link rel="stylesheet" href="assets/css/local.css">

    <!-- Reveal.js colors/theme -->
    <link rel="stylesheet" href="assets/reveal.js/reveal.css">
    <link rel="stylesheet" href="assets/reveal.js/theme/blood.css">

    <!-- Code syntax highlighting -->
    <link rel="stylesheet" href="assets/reveal.js/plugin/highlight/zenburn.css">
    <link rel="stylesheet" href="assets/reveal.js/plugin/highlight/monokai.css">

    <!-- Printing and PDF exports -->
    <script>
      var link = document.createElement( 'link' );
      link.rel = 'stylesheet';
      link.type = 'text/css';
      link.href = window.location.search.match( /print-pdf/gi ) ? 'assets/css/print/pdf.css' : 'assets/css/print/paper.css';
      document.getElementsByTagName( 'head' )[0].appendChild( link );
    </script>
  </head>

  <body>

    <div class="reveal">

      <div class="footer">
        <div class="logo">
          <img src="assets/figures/ParresiaLogoNov2015_1034x304.png">
        </div>
        <div class="webpage">
          https://bbanerjee.github.io/ParSim/
        </div>
        <div class="twitter">
          email: b.banerjee.nz@gmail.com 
        </div>
      </div>

      <!-- Any section element inside of this container is displayed as a slide -->
      <div class="slides">

        <!-- Title -->
        <section>
          <br/>
          <br/>
          <h2>
            Revisiting tabular material models
          </h2>
          <br/>
          <p>
            Biswajit Banerjee <br/>
            Parresia Research Limited
          </p>
          <br/>
          <br/>
          <small>Mumbai</small>
          <small>July 2022</small>
        </section>

        <!-- About -->
        <section data-state="about_slide">
          <h3> About Parresia </h3>
          <div>
            <ul style="width: 60%;">
              <li>
                Consulting and contract engineering model/software development
              </li>
              <li>
                Material model specialists: rocks, fiber composites, elastomers, soils,
                metals and alloys, bones and tissue
              </li>
              <li>
                 Finite elements, material point method, discrete elements
              </li>
              <li>
                 Based in Auckland, NZ
              </li>
            </ul>
            <img width="30%" style="float: right" src="assets/figures/foam_expand.png">
          </div>

        </section>

        <!-- Outline -->
        <section data-markdown data-state="outline_slide">
          <textarea data-template>
            ### Outline

            - The Material Point method (MPM)
            - Interpolating tabular material data
              - Linear interpolation, radial basis functions, kriging
            - Support vector regression
            - Multilayer peceptron neural networks
            - MPM simulations
            - Remarks
          </textarea>
        </section>

        <!-- MPM -->
        <section data-state="mpm_slide_1">
          <h3> The Material Point method (MPM) </h3>
          <!-- <div style="background:white"> -->
          <div>
            <div>
              <canvas id="mpm-simulation" height="500" width="500"></canvas>
              <input name="restartButton" type="button" value="Next step" onclick="mpmSimulation.restartAnimation()" />
            </div>
          </div>
        </section>

        <section data-state="scatter_slide_4">
          <h3> Patch-based decomposition </h3>
          <div>
          <div style="border:1px solid white; width: 48%; float: left; background: white;">
            <div>
              <input name="restartScatter" type="button" value="Scatter particles" onclick="particleScatterGhost.restartAnimation()" />
            </div>
            <div>
              <canvas id="particle-scatter-ghost" height="500" width="500"></canvas>
            </div>
          </div>
          <div style="border:1px solid white; width: 48%; float: right; background: white">
            <div>
              <input name="restartExchange" type="button" value="Exchange ghost particles" onclick="particlePlimpton.restartAnimation()" />
            </div>
            <div>
              <canvas id="particle-exchange-plimpton" height="500" width="500"></canvas>
            </div>
          </div>
          </div>
        </section>

        <section data-markdown data-state="mpm_slide_2">
          <textarea data-template>

            ### MPM advantages
            <p>
              <div id="box_left" style="border:1px solid white; width: 67%; float: left;">
                <ul>
                  <li> Particles are Lagrangian and carry state - needed for history-dependent computations </li>
                  <li>Eulerian approaches tend to be dispersive and cannot be used </li>
                  <li>Finite element methods cannot deal with excessive deformation without remeshing
                      and the associated dispersion of internal variables </li>
                  <li>In many situations, contact is "free" </li>
                </ul>
              </div>
              <div id="box_right" style="border:1px solid white; width: 31%; float: right;">
                 <img src="assets/figures/assembly.png"/>

                 Large deformation study 
                 with numerous contacts 
                 between parts
              </div>
            </p>
          </textarea>
        </section>

        <!-- Tabular models -->

        <section data-markdown data-state="tabular_data">
          <textarea data-template>
            ### Tabular material data 
            <p>
            <div style="border:1px solid white; width: 60%; float: left;">
              <img src="assets/figures/table_text.png"/>
            </div>
            <div style="border:1px solid white; width: 39%; float: right;">
              <img src="assets/figures/table_plot.png"/>
            </div>
            <!--<div id="citation">-->
            <div style="text-align: left">
              Tabular data rarely sample the phase space uniformly.
              Independent variables in modern micromechanical models can 
              be high-dimensional (O(10)).
            </div>
            </p>

          </textarea>
        </section>

        <section data-markdown data-state="interpolation_history">
          <textarea data-template>
            ### Interpolating multivariable data
            <p>
              <div id="box_left" style="border:1px solid white; width: 68%; float: left; font-size: 30px">
                <ul>
                  <li> Long history of linear/quadratic interpolation
                  <li> 1964: Applied to data that can be mapped topologically to a Cartesian grid
                       <span class="blue-text" style="font-size: 16px">
                       Ferguson (1964). "Multivariable curve interpolation". Journal of the ACM (JACM) 11.2
                       </span> </li>
                  <li> 1967: Coons patches (for high dimensions) were designed for parameteric interpolation
                       <span class="blue-text" style="font-size: 16px">
                       Coons (1967). "Surfaces for computer-aided design of space forms." Tech. Rep. MIT.
                       </span> </li>
                  <li> 1982: Franke published a survey of interpolation methods
                       <span class="blue-text" style="font-size: 16px">
                       Franke (1982). "A critical comparison of some methods for interpolation of scattered data."
                       Mathematics of computation 38.157
                       </span> </li>
                  <li> At present, distance weighted methods (radial basis functions/
                       kriging) for high-dimensional data and linear interpolation are popular </li>
                </ul>
              </div>
              <div id="box_right" style="border:1px solid white; width: 30%; float: right; text-align: left; font-size: 30px">
                For more detail see the draft paper on my website.
                <ul>
                  <li> In mechanics, we will need to call interpolation routines <span class="red-text">millions of times</span> during a computation </li>
                  <li> Need <span class="red-text">fast methods</span> that do not require special mathematical function calls
                </ul>
              </div>
            <p>
          </textarea>
        </section>

        <section data-state="analytical_expressions">
          <h3>Analytical expressions </h3>
            <p>
              <div id="box_left" style="border:1px solid white; width: 100%; float: left; font-size: 30px;">
                $$
                  \definecolor{par_green}{RGB}{174, 209, 79}
                  \definecolor{par_blue}{RGB}{51, 153, 255}
                  \definecolor{par_red}{RGB}{255, 153, 102}
                  K(I_1, \varepsilon^p_v) = f_K \left[ \left\{ \color{par_green}{b_0} + b_1 \exp(-b_2/|I_1|)\right\} - 
                                               \color{par_red}{b_3\exp(-b_4/|\varepsilon^p_v|)} \right]
                $$
              </div>
              <div id="box_right" style="border:1px solid white; width: 100%; float: left;">
                <ul>
                  <li> Needs to be <span class="yellow-text">fitted to data</span>.  
                       The larger the number of independent variables
                       the more difficult this becomes </li>
                  <li> Often the model will not fit the data at all and will have to be 
                       <span class="yellow-text">redesigned</span>, 
                       leading to a significant amount of wasted time </li>
                  <li> Many models require the evaluation of <span class="yellow-text">special functions </span>
                       which can be expensive when exercised millions of times </li>
                </ul>
              </div>
            </p>
        </section>

        <section data-state="lin_interp">
          <h3> Linear interpolation </h3>
          <div id="box_left" style="border:1px solid white; width: 60%; float: left; font-size: 30px;">
            $$
              s = \frac{\alpha_0 - \alpha_k}{\alpha_{k+1} - \alpha_k}~,~~ k=1,2,\dots,N-1 ~,~~ s \in [0,1]
            $$
            $$
             \begin{aligned}
             t_1 &= \frac{\varepsilon_0 - \varepsilon_{j,k}}{\varepsilon_{j,k+1} - \varepsilon_{j,k}}~,~~ k=1,2,\dots,M_j-1 \\
             t_2 &= \frac{\varepsilon_0 - \varepsilon_{j+1,k}}{\varepsilon_{j+1,k+1} - \varepsilon_{j+1,k}}~,~~ k=1,2,\dots,M_{j+1}-1
             \end{aligned}
            $$
            $$
             \begin{aligned}
             p_1 &= (1 - t_1) p_{j,k} + t_1 p_{j,k+1} \\
             p_2 &= (1 - t_2) p_{j+1,k} + t_2 p_{j+1,k+1}
             \end{aligned}
            $$
            $$
             p_0 = (1 - s) p_1 + s p_2
            $$
          </div>
          <div id="box_right" style="border:1px solid white; width: 39%; float: right; background: white;">
            <img src="assets/figures/table_interpolation_step1.svg" height="200"/>
            <img src="assets/figures/table_interpolation_step2.svg" height="200"/>
            <img src="assets/figures/table_interpolation_step3.svg" height="200"/>

            <br/><span class="blue-text">Problematic in high dimensions</span>
          </div>
        </section>

        <section data-state="radial_basis">
          <h3> Radial-basis functions </h3>
          <div id="box_left" style="border:1px solid white; width: 60%; float: left; font-size: 26px;">
            $$
              \newcommand{\Bx}{\mathbf{x}}
              \newcommand{\Br}{\mathbf{r}}
              \begin{aligned}
              p(\Bx) & = \sum_{i=1}^n \lambda_i \varphi(\|\Bx - \Bx_i\|) 
                     = \sum_{i=1}^n \lambda_i \varphi(\|\Br_i\|)
                     = \sum_{i=1}^n \lambda_i \varphi(r_i) \\
              & \|\Br\| = r := \sqrt{\Br \cdot \Br} \\
              & \varphi(\|{\Bx - \Bx_i}\|) = \varphi(r) = \sqrt{1 + (\xi r)^2}
              \end{aligned}
            $$
            $$
              \begin{bmatrix}
                \varphi(r_{11}) & \varphi(r_{21}) & \dots & \varphi(r_{n1}) \\
                \varphi(r_{12}) & \varphi(r_{22}) & \dots & \varphi(r_{n2}) \\
                \vdots          & \vdots          & \ddots & \vdots \\
                \varphi(r_{1n}) & \varphi(r_{2n}) & \dots & \varphi(r_{nn}) 
              \end{bmatrix}
              \begin{bmatrix} \lambda_1 \\ \lambda_2 \\ \vdots \\ \lambda_n \end{bmatrix}
              = 
              \begin{bmatrix} p(\Bx_1) \\ p(\Bx_2) \\ \vdots \\ p(\Bx_n) \end{bmatrix}
            $$
            $$
              p(\Bx_0) = \sum_{i=1}^n \lambda_i \varphi(\|{\Bx_0 - \Bx_i}\|)
            $$
          </div>
          <div id="box_right" style="border:1px solid white; width: 39%; float: right;">
            <p style="background: white;">
            <img src="assets/figures/table_nearest_neighbor.svg" height="400"/>
            </p>
            <div style="font-size: 26px;">
            Matrix size can be reduced by a nearest neighbor search.
            </div>
          </div>
        </section>
        
        <section data-state="kriging">
          <h3> Kriging </h3>
          <div id="box_left" style="border:1px solid white; width: 70%; float: left; font-size: 26px;">
            $$
              p(\Bx) = \sum_{i=1}^n \lambda_i p(\Bx_i) ~,\quad \sum_{i=1}^n \lambda_i = 1
            $$
            $$
              \newcommand{\Blambda}{\boldsymbol{\lambda}}
              \underset{\Blambda}{\text{minimize}}\quad \mathbb{E}\left[ \bigl(P^\star(\Bx) - P(\Bx)\bigr)^2 \right]
            $$
            $$
              \begin{bmatrix}
                \gamma(\Bx_1, \Bx_1) & \gamma(\Bx_1, \Bx_2) & 
                  \dots & \gamma(\Bx_1, \Bx_n)] & 1\\ 
                \gamma(\Bx_2, \Bx_1) & \gamma(\Bx_2, \Bx_2) & 
                  \dots & \gamma(\Bx_2, \Bx_n)] & 1\\ 
                \vdots & \vdots & \ddots & \vdots & \vdots \\
                \gamma(\Bx_n, \Bx_1) & \gamma(\Bx_n, \Bx_2) & 
                  \dots & \gamma(\Bx_n, \Bx_n) & 1 \\
                1 & 1 & \dots & 1 & 0
              \end{bmatrix}
              \begin{bmatrix} \lambda_1 \\ \lambda_2 \\ \vdots \\ \lambda_n \\ \mu \end{bmatrix}
              = 
              \begin{bmatrix}
                \gamma(\Bx_0, \Bx_1) \\ \gamma(\Bx_0, \Bx_2) \\ 
                  \vdots \\ \gamma(\Bx_0, \Bx_n) \\ 1
              \end{bmatrix}
            $$
            with semivariances $\gamma$ determined from variograms
          </div>
          <div id="box_right" style="border:1px solid white; width: 29%; float: right;">
            <p style="background: white;">
            <img src="assets/figures/table_nearest_neighbor.svg" height="400"/>
            </p>
            <div style="font-size: 26px;">
            Matrix size can be reduced by a nearest neighbor search.
            </div>
          </div>
        </section>

        <section data-state="experimental_data">
            <h3> Experimental data </h3>
            <p>
              <div id="box_left" style="border:1px solid white; width: 49%; float: left; font-size: 30px; background: white;">
                <img src="assets/figures/DrySand_Expt_Pressure_VolStrain.svg"/>
              </div>
              <div id="box_right" style="border:1px solid white; width: 49%; float: right; background: white;">
                <img src="assets/figures/DrySand_Expt_BulkModulus_ElasticVolStrain.svg"/>
              </div>
            <p>
        </section>

        <section data-state="comparison_interp">
            <h3> Comparison of methods </h3>
            <p>
              <div id="box_left" style="border:1px solid white; width: 34%; float: left; font-size: 30px; background: white;">
                <img src="assets/figures/Fox_DrySand_BulkModulus_direct_linear.svg"/>
                <span class="blue-text">Linear interpolaton</span>
              </div>
              <div id="box_left" style="border:1px solid white; width: 32%; float: right; background: white;">
                <img src="assets/figures/Fox_DrySand_BulkModulus_Strain_RBF_TwoCurves.svg"/>
                <span class="blue-text">Radial basis function</span>
              </div>
              <div id="box_left" style="border:1px solid white; width: 32%; float: right; background: white;">
                <img src="assets/figures/Fox_DrySand_BulkModulus_Strain_Kriging_TwoCurves.svg"/>
                <span class="blue-text">Kriging</span>
              </div>
            <p>
        </section>

        <!-- Mechanics & Material models -->

        <section data-state="mechanics">
            <h3> Interlude: A bit of mechanics </h3>
            <p>
              Cap plasticity phenomenology
              <div>
              <div id="box_left" style="border:1px solid white; width: 49%; float: left; background: white">
                <img src="assets/figures/ClosestPointsAndNormals3D.svg" height="360"/> 
              </div>
              <div id="box_right" style="border:1px solid white; width: 49%; float: right; background: white">
                <img src="assets/figures/ClosestPointsAndNormals_PQ_2000.svg" height="360"/>
              </div>
              </div>
            </p>
        </section>

        <section data-state="material_model">
            <h3> Does a variable bulk modulus matter? </h3>
            <p>
              <div>
              <div id="box_left" style="border:1px solid white; width: 66%; float: left; background: white">
                <img src="assets/figures/LoadUnload_Arenisca3_Fixed_vs_VariableK_pq.svg" height="360"/> 
              </div>
              <div id="box_right" style="border:1px solid white; width: 33%; float: right; background: white">
                <img src="assets/figures/LoadUnload_Arenisca3_Fixed_vs_VariableK_eps_p.svg" height="360"/>
              </div>
              </div>
            </p>
            <div style:"font-size: 22px;">
              Even for a model that appears to be well-approximated by a constant bulk modulus, a variable
              bulk modulus can lead to a different final state
            </div>
        </section>

        <section data-state="punch_indentation">
            <h3> A more complex simulation </h3>
            <p>
              <div id="box_left" style="border:1px solid white; width: 100%; float: left; background: white">
                <img src="assets/figures/PunchIndent_Arenisca3_Fixed_vs_VariableK_vel_2x_q.svg" height="400" /> 
              </div>
            </p>
            <div style:"font-size: 22px;">
              In this simulation of a punch indenting into sand, 
              small differences in the bulk modulus model clearly lead to different final configurations
            </div>
        </section>

        <!-- Support vector regression -->
        <section data-state="svr_equations">
          <h3> Support vector regression </h3>
          <p>
            <div id="box_left" style="border:1px solid white; width: 69%; float:left; font-size: 26px">
              $$
                \newcommand{\Bphi}{\boldsymbol{\varphi}}
                \newcommand{\Bw}{\mathbf{w}}
                \begin{aligned}
                  & \text{Original}: y = f(\Bx) =  \Bw \cdot \Bphi(\Bx) + b \\
                  & \text{Dual}: y = f(\Bx) = \sum_{i=1}^m (\lambda_i^\star - \lambda_i) K(\Bx_i, \Bx) + b \\
                  & \qquad               K(\Bx_i,\Bx) = \Bphi(\Bx_i) \cdot \Bphi(\Bx)
                \end{aligned}
              $$
              $$
                \begin{aligned}
                  \underset{\Blambda,\Blambda^\star}{\text{minimize}}\quad &
                  \tfrac{1}{2}
                  \sum_{i,j=1}^m (\lambda_i - \lambda_i^\star) K(\Bx_i, \Bx_j) (\lambda_j - \lambda_j^\star) 
                   + \epsilon \sum_{i=1}^m (\lambda_i + \lambda_i^\star) \\
                   & \quad + \sum_{i=1}^m y_i(\lambda_i - \lambda_i^\star) \\
                  \text{subject to}\quad &
                     \begin{cases}
                       \sum_{i=1}^m (\lambda_i - \lambda_i^\star) = 0 \\
                       \lambda_i, \lambda_i^\star \in [0, C] ~,~~ i = 1 \dots m \,.
                     \end{cases}
                \end{aligned}
              $$
              $$
                K(\Bx_i, \Bx_j) = \exp\left[-\frac{(\Bx_i-\Bx_j)\cdot(\Bx_i-\Bx_j)}{d\sigma^2}\right]
              $$
            </div>
            <div id="box_right" style="border:1px solid white; width: 29%; float: right; background: white;">
               <img src="assets/figures/Fox_DrySand_Crush_Curve.svg" height="250" /> 
               <img src="assets/figures/DrySand_Expt_BulkModulus_ElasticVolStrain.svg" height="250"/>
            </div>
          </p>
        </section>

        <section data-state="svr_curve_fit">
          <h3> SVR curve fits </h3>
          <div>
            <div style="border:1px solid white; width: 49%; float:left; background: white;">
              <img src="assets/figures/Fox_DrySand_PressureCrushCurve_SVR_C_Normal.svg" height="450"/>
              <span class="blue-text">Crush curve fits</span>
            </div>
            <div style="border:1px solid white; width: 49%; float: right; background: white;">
              <img src="assets/figures/Fox_DrySand_BulkModulus_ElasticStrain_SVR_e0001_scaled_bootstrap.svg" height="450"/>
              <span class="blue-text">Bulk modulus fits</span>
            </div>
          </div>
        </section>

        <section data-state="svr_predictions">
          <h3> SVR predictions </h3>
          <div>
            <div style="border:1px solid white; width: 49%; float:left; background: white;">
              <img src="assets/figures/Fox_DrySand_Pressure_TotalStrain_Tension_Predict_SVR_1_001_scaled_cv.svg" height="450"/>
            </div>
            <div style="border:1px solid white; width: 49%; float: right; background: white;">
              <img src="assets/figures/Fox_DrySand_BulkModulus_TotalElasticStrain_Tension_Predict_SVR_1_001_scaled_cv.svg " height="450"/>
            </div>
          </div>
        </section>

        <!-- Multilayer perceptron -->
        <section data-state="mlp_slide">
          <h3>What about neural networks?</h3>
          <p>
            <div id="box_left" style="border:1px solid white; width: 40%; float:left; background: white">
              <img src="assets/figures/multilayer_perceptron.png" width="500"/>
            </div>
            <div id="box_right" style="border:1px solid white; width: 59%; float:right; text-align: left; font-size: 26px;">
                $$
                \quad y = f(\mathbf{x}) = g_2({\color{par_blue}\boldsymbol{W}_2
                \cdot \mathbf{g}_1(}{\color{par_red}\boldsymbol{W}_1
                \cdot \mathbf{g}_0(}{\color{par_green}\boldsymbol{W}_0
                \cdot\mathbf{x} + \mathbf{b}_0}{\color{par_red}) + \mathbf{b}_1}{\color{par_blue}) + \mathbf{b}_2})
                $$

              <ul>
                <li> Dense multilayer perceptron (MLP) </li>

                <li> Formulated as a least-squares minimization problem
                which is solved using steepest descent (sometimes
                stochastic) </li>

                <li> Gradients are computed using reverse
                automatic differentiation (backpropagation) </li>

                <li> With TensorFlow 2+ and Keras Python frontend, fitting data
                with neural nets has become straightforward </li>

                <li> For mechanics applications, at least double precision fits needed </li>
              </ul>  
            </div>
          <p>
        </section>

        <section data-state="recall_expt_data">
          <h3> Recall experimental data </h3>
          <p>
            <div id="box_left" style="border:1px solid white; width: 49%; float:left; background: white">
              <img height="400" align="center" src="assets/figures/DrySand_Expt_BulkModulus_ElasticVolStrain.svg"/>
              <br/>
              <span class="blue-text"> Bulk modulus curves </span>
            </div>
            <div id="box_right" style="border:1px solid white; width: 49%; float:right; background: white">
              <img height="400" align="center" src="assets/figures/DrySand_Crush_Curve_Porosity.svg"/>
              <br/>
              <span class="blue-text"> Crush curve </span>
            </div>
          </p>
          <div style="font-size: 24px;">
            Assumes additive decomposition of volumetric strains into elastic and plastic parts <br/>
            Porosity = 0.325 - volumetric plastic strain
          </div>
        </section>

        <section data-state="crush_curve">
          <h3> Crush curve fit </h3>
          <p>
            <div id="box_left" style="border:1px solid white; width: 40%; float:left; background: white;">>
                <img src="assets/figures/Fox_DrySand_CrushCurve_MLP_two_layer_128_800.svg" width="500"/>
            </div>
            <div id="box_right" style="border:1px solid white; width: 59%; float:right; font-size: 30px;"> 
              <ul>
                <li>Two layer neural network: 
                  <ul>
                    <li>32 neurons in the first hidden layer,</li>
                    <li>64 neurons in the second hidden layer,</li>
                    <li>Fully connected</li>
                  </ul>
                </li>
                <li>ReLU activation functions</li>
                <li>Gradient descent batch size: 128. </li>
                <li>Gradient descent epochs: 800</li>
                <li>Double precision</li>
                <li>No bootstrapping</li>
            </div>
          </p>
        </section>

        <section data-state="bulk_modulus">
          <h3> Bulk modulus fits: Models A and B </h3>
          <p>
            <div id="box_left" style="border:1px solid white; width: 49%; float:left; background: white; font-size:24px;">
              <img src="assets/figures/Fox_DrySand_Bulk_ElasticStrain_Tension_MLP_32_800_scaled.svg" height="370"/>
              <br/>
              <span class="red-text">Model A:</span>
              <span class="blue-text">
                
                  Fully connected, three layer neural network;
                  64 sigmoid neurons in the first hidden layer;
                  32 sigmoid neurons in the second layer;
                  32 ReLU neurons in the third layer;
                  Double precision and bootstrapping
              </span>
            </div>
            <div id="box_right" style="border:1px solid white; width: 49%; float:right; background: white; font-size:24px;">
              <img src="assets/figures/Fox_DrySand_Bulk_ElasticStrain_Tension_MLP_32_400_scaled_relu.svg" height="370"/>
              <br/>
              <span class="red-text">Model B: </span>
              <span class="blue-text">
                
                  Fully connected, three layer neural network;
                  64 ReLU neurons in the first layer;
                  32 ReLU neurons in the second layer;
                  32 ReLU neurons in the third layer;
                  Double precision and bootstrapping
              </span>
            </div>
          </p>
        </section>

        <section data-state="bulk_modulus">
          <h3> Predictions: Models A and B </h3>
          <p>
            <div id="box_left" style="border:1px solid white; width: 49%; float:left; background: white; font-size:24px;">
                <img src="assets/figures/Fox_DrySand_Bulk_ElasticStrain_Tension_MLP_Pred_32_800_scaled.svg" height="400"/>
                <br/>
                <span class="blue-text"> Model A </span>
            </div>
            <div id="box_right" style="border:1px solid white; width: 49%; float:right; background: white; font-size:24px;">
                <img src="assets/figures/Fox_DrySand_Bulk_ElasticStrain_Tension_MLP_Pred_32_400_scaled_relu.svg" height="400"/>
                <br/>
                <span class="blue-text"> Model B </span>
            </div>

            <div style="font-size: 24px;">
              <ul>
                <li>Better predictions with ReLU activation </li>
                <li>Sharp transitions are not predicted well </li>
              </ul>
            </div>
          </p>
        </section>

        <!-- Simulation -->
        <section data-state="punch_impact">
          <h3>Punch impact</h3>
          <p>
            <div>
              <div style="border:0px solid white; width: 33%; float:left; font-size:24px;">
                <img src="assets/figures/PunchIndent_MLP_vs_Table_K07.png" height="250"/>
              </div>
              <div style="border:0px solid white; width: 34%; float:right; font-size:24px;">
                <img src="assets/figures/PunchIndent_MLP_vs_Table_K26.png" height="250"/>
              </div>
              <div style="border:0px solid white; width: 33%; float:right; font-size:24px;">
                <img src="assets/figures/PunchIndent_MLP_vs_Table_K14.png" height="250"/>
              </div>
            </div>
            <div>
              <div style="border:0px solid white; width: 33%; float:left; font-size:24px;">
                 <img src="assets/figures/PunchIndent_MLP_vs_Table_K33.png" height="250"/>
              </div>
              <div style="border:0px solid white; width: 66%; float:right; font-size:30px;">
                 Left: Neural network model; Right: Linear interpolation
                 <ul>
                   <li>Estimated bulk moduli are different</li>
                   <li>Depth of penetration approximately the same</li>
                 </ul>
              </div>
            </div>
          </p>
        </section>

        <section data-markdown data-state="uxo_model">
          <textarea data-template>
            ### Unexploded ordnance (UXO)
            <p>
              <div id="box_left" style="border:1px solid white; width: 49%; float:left; font-size:24px;">
                <img src="assets/figures/UXO_OpenSCAD.svg" width="500"/>
              </div>
              <div id="box_right" style="border:1px solid white; width: 49%; float:right; font-size:24px;">
                <img src="assets/figures/UXO_Table_transparent06.png" width="500"/>
              </div>
            </p>
          </textarea>
        </section>

        <section data-markdown data-state="uxo_depth">
          <textarea data-template>
            ### Penetration depth
            <p>
              <div id="box_left" style="border:1px solid white; width: 49%; float:left; font-size:24px;">
                <img src="assets/figures/UXO_Table_vel22.png" height="480"/>
                <br/>
                Linear interpolation
              </div>
              <div id="box_right" style="border:1px solid white; width: 49%; float:right; font-size:24px;">
                <img src="assets/figures/UXO_MLP_vel22.png" height="480"/>
                <br/>
                Neural network model
              </div>
            </p>
          </textarea>
        </section>

        <!-- Remarks -->
        <section data-markdown data-state="remarks">
          <textarea data-template>
            ### Remarks
            * Linear interpolation produces reasonable results for ~~low-dimensional~~ problems   
            * Radial basis functions/kriging can be ~~expensive~~ and are not suitable for models
              that are evaluated billions of times in a simulation
            * Support vector repression fails to produce models that generalize well
            * Neural networks will ~~fail to generalize~~ if sufficient coverage of the
              parameter space is not available in the input data
            * A significant amount of tuning (~~network topology and parameters~~)
              is needed to produce reasonable results
          </textarea>
        </section>
          
      </div>

    </div>

    <script src="assets/js/head.min.js"></script>
    <script src="assets/reveal.js/reveal.js"></script>
    <script src="assets/reveal.js/plugin/math/math.js"></script>
    <script src="assets/reveal.js/plugin/markdown/markdown.js"></script>
    <script src="assets/reveal.js/plugin/highlight/highlight.js"></script>
    <script src="assets/reveal.js/plugin/zoom/zoom.js"></script>
    <script src="assets/js/d3.v4.min.js"></script>
    <script src="assets/js/colorbrewer.min.js"></script>
    <script src="assets/js/particleScatter.js"></script>
    <script src="assets/js/particleScatterGhost.js"></script>
    <script src="assets/js/particleExchange.js"></script>
    <script src="assets/js/seedrandom.min.js"></script>
    <script src="assets/js/particleMigrate.js"></script>
    <script src="assets/js/particleMotion.js"></script>
    <script src="assets/js/particlePlimpton.js"></script>
    <script src="assets/js/mpmSimulation.js"></script>

    <script>

      // Full list of configuration options available at:
      // https://github.com/hakimel/reveal.js#configuration
      Reveal.initialize({
        width:1300,
        controls: true,
        controlsTutorial: false,
        progress: true,
        history: true,
        center: false,
        slideNumber: 'c/t',
        zoomKey: "ctrl",

        //transition: 'linear', // none/fade/slide/convex/concave/zoom
        transition: 'convex', // none/fade/slide/convex/concave/zoom
       
        math: {
          mathjax : 'assets/MathJax/MathJax.js',
          config: 'TeX-AMS_HTML-full'
        },

        plugins: [ RevealMath, RevealMarkdown, RevealHighlight, RevealZoom ],

        // Optional reveal.js plugins
        dependencies: [
          { src: 'assets/js/classList.js' },
          { src: 'assets/js/classList.js', condition: function() { return !document.body.classList; } },
          { src: 'assets/reveal.js/plugin/markdown/markdown.js', condition: function() { return !!document.querySelector( '[data-markdown]' ); } },
          // { src: 'assets/reveal.js/plugin/zoom/zoom.js', async: true },
        ]
      });

    </script>
    <script type="text/x-mathjax-config">
      MathJax.Hub.Config({ TeX: { extensions: ["color.js"] }});
    </script>


  </body>
</html>
