<!doctype html>
<html lang="en">

  <head>
    <meta charset="utf-8">

    <title>
      The Vaango MPM + ICE code design and possible GUI implementations: 
      advantages and issues
    </title> 

    <meta name="description" content="@SankhyaSutra Labs, Bangalore, July 2022">
    <meta name="author" content="Biswajit Banerjee">

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <!-- My colors -->
    <link rel="stylesheet" href="assets/css/local.css">

    <!-- Reveal.js colors/theme -->
    <link rel="stylesheet" href="assets/reveal.js/reveal.css">
    <link rel="stylesheet" href="assets/reveal.js/theme/blood.css">

    <!-- Code syntax highlighting -->
    <link rel="stylesheet" href="assets/reveal.js/plugin/highlight/zenburn.css">
    <link rel="stylesheet" href="assets/reveal.js/plugin/highlight/monokai.css">

    <!-- Printing and PDF exports -->
    <script>
      var link = document.createElement( 'link' );
      link.rel = 'stylesheet';
      link.type = 'text/css';
      link.href = window.location.search.match( /print-pdf/gi ) ? 'assets/css/print/pdf.css' : 'assets/css/print/paper.css';
      document.getElementsByTagName( 'head' )[0].appendChild( link );
    </script>
  </head>

  <body>

    <div class="reveal">

      <div class="footer">
        <div class="logo">
          <img src="assets/figures/ParresiaLogoNov2015_1034x304.png">
        </div>
        <div class="webpage">
          https://bbanerjee.github.io/ParSim/
        </div>
        <div class="twitter">
          email: b.banerjee.nz@gmail.com 
        </div>
      </div>

      <!-- Any section element inside of this container is displayed as a slide -->
      <div class="slides">

        <!-- Title -->
        <section>
          <br/>
          <br/>
          <h2>
            <div class="emph">Vaango</div>:  design, advantages, and issues
          </h2>
          <br/>
          <p>
            Biswajit Banerjee <br/>
            Parresia Research Limited
          </p>
          <br/>
          <br/>
          <small>Bengaluru</small>
          <small>July 2022</small>
        </section>

        <!-- About -->
        <section data-state="about_slide">
          <h3> About Parresia </h3>
          <div>
            <ul style="width: 60%;">
              <li>
                Consulting and contract engineering model/software development
              </li>
              <li>
                Material model specialists: rocks, fiber composites, elastomers, soils,
                metals and alloys, bones and tissue
              </li>
              <li>
                 Finite elements, material point method, discrete elements
              </li>
              <li>
                 Based in Auckland, NZ
              </li>
            </ul>
            <img width="30%" style="float: right" src="assets/figures/foam_expand.png">
          </div>

        </section>

        <!-- Outline -->
        <section data-markdown data-state="outline_slide">
          <textarea data-template>
            ### Outline

            - The Material Point method (MPM)
            - Patch-based decomposition of the physical domain
            - The parallel computational framework

            <br/><div class="emph">If time permits</div>:

            - How material models are implemented
            - Inputs and visualization of the results
            - Remarks
          </textarea>
        </section>

        <!-- MPM -->
        <section data-state="mpm_slide_1">
          <h3> The Material Point method (MPM) </h3>
          <!-- <div style="background:white"> -->
          <div>
            <div>
              <canvas id="mpm-simulation" height="500" width="500"></canvas>
              <input name="restartButton" type="button" value="Next step" onclick="mpmSimulation.restartAnimation()" />
            </div>
          </div>
        </section>

        <section data-markdown data-state="mpm_slide_2">
          <textarea data-template>

            ### MPM advantages
            <p>
              <div id="box_left" style="border:1px solid white; width: 67%; float: left;">
                <ul>
                  <li> Particles are Lagrangian and carry state - needed for history-dependent computations </li>
                  <li>Eulerian approaches tend to be dispersive and cannot be used </li>
                  <li>Finite element methods cannot deal with excessive deformation without remeshing
                      and the associated dispersion of internal variables </li>
                  <li>In many situations, contact is "free" </li>
                </ul>
              </div>
              <div id="box_right" style="border:1px solid white; width: 31%; float: right;">
                 <img src="assets/figures/assembly.png"/>

                 Large deformation study 
                 with numerous contacts 
                 between parts
              </div>
            </p>
          </textarea>
        </section>

        <!-- Patch-based operations -->

        <section data-state="scatter_slide_4">
          <h3> Patch-based decomposition </h3>
          <div>
          <div style="border:1px solid white; width: 48%; float: left; background: white;">
            <div>
              <input name="restartScatter" type="button" value="Scatter particles" onclick="particleScatterGhost.restartAnimation()" />
            </div>
            <div>
              <canvas id="particle-scatter-ghost" height="500" width="500"></canvas>
            </div>
          </div>
          <div style="border:1px solid white; width: 48%; float: right; background: white">
            <div>
              <input name="restartExchange" type="button" value="Exchange ghost particles" onclick="particlePlimpton.restartAnimation()" />
            </div>
            <div>
              <canvas id="particle-exchange-plimpton" height="500" width="500"></canvas>
            </div>
          </div>
          </div>
        </section>

        <section data-state="scatter_slide_6">
          <h3> Particle relocation </h3>
          <div align="center" style="border:1px solid black; background: white">
            <div>
              <input name="restartMigrate" type="button" value="Migrate particles across patches" onclick="particleMigrate.restartAnimation()" />
            </div>
            <div>
              <canvas id="particle-migrate" height="500" width="500"></canvas>
            </div>
          </div>
        </section>

        <!-- Parallel computational framework -->

        <section data-markdown data-state="uintah_slide">
          <textarea data-template>
            ### Uintah computational framework 

            <p>
            <div style="border:1px solid white; width: 68%; float: left;">
              <ul>
                <li> 
                  <span class="blue-text"> Vaango </span> is directly descended from the
                   Uintah computational framework developed at the University of Utah starting in 1998
                   (<span class="blue-text"> uintah.utah.edu </span>) 
                </li>
                <li> 
                   Forked in 2014 after support for MPM decreased
                </li>
                <li> 
                   Designed with rectangular Cartesian grids in mind
                </li>
                <li> 
                   Asynchronous many-task (AMT) framework 
                </li>
              </ul>
            </div>
            <div style="border:1px solid white; width: 29%; float: right;">
              <img src="assets/figures/fire-container-explosion-scirun.png"/>
            </div>
            <div id="citation">
              Vivek Sarkar (1987) Partitioning and scheduling parallel programs for execution on 
              multiprocessors, PhD Dissertation, Stanford Univ.
              JD de St Germain, J McCorquodale, SG Parker, CR Johnson (2000) 
              Uintah: A massively parallel problem solving environment.
            </div>
            </p>

          </textarea>
        </section>

        <section data-markdown data-state="amt_slide">
          <textarea data-template>
            ### Asynchronous many-task system
            <p>
              <div id="box_left" style="border:1px solid white; width: 68%; float: left;">
                <ul>
                  <li> Free application developer from worrying about partitioning and communication </li>
                  <li> Partitions the problem into tasks </li>
                  <li> A task scheduler builds the task graph and makes sure all dependencies are 
                       satisfied before a task is run </li>
                  <li> Multiple data "warehouses" are used to store and retrieve state needed for a
                       computation </li>
                  <li> Tasks do not need to run in lock-step </li>
                </ul>
              </div>
              <div id="box_right" style="border:1px solid white; width: 30%; float: right;">
                <img src="assets/figures/asynchronous_task_runtime.svg"/>
              </div>
            <p>
          </textarea>
        </section>

        <section data-state="user_view">
          <h3>Application developer's view </h3>
            <p>
              <div id="box_left" style="border:1px solid white; width: 68%; float: left;">
                <ul>
                 <li> Creation of an input file that contains domain and geometry, 
                      patch definition, algorithm to be exercised (MPM/MPM-ICE), 
                      material models, contact conditions, boundary conditions </li>
                 <li> Declaration of various tasks - what the task requires, modifies, or computes </li>
                 <li> Implementation of the task logic </li>
                </ul>
              </div>
              <div id="box_right" style="border:1px solid white; width: 30%; float: right;">
                <pre> <code data-trim data-noescape>  
&lt;?xml version='1.0' encoding='ISO-8859-1' ?&gt; 
&lt;Uintah_specification&gt;
  &lt;Meta&gt;
    &lt;title&gt;Hydrostatic compression (tabular)&lt;/title&gt;
  &lt;/Meta&gt;
  &lt;SimulationComponent type=&quot;mpm&quot; /&gt; 
  &lt;Time&gt;
    &lt;maxTime&gt;  1.0    &lt;/maxTime&gt;
    &lt;delt_min&gt; 1.0e-6 &lt;/delt_min&gt;
  &lt;/Time&gt;
  &lt;DataArchiver&gt;
    &lt;filebase&gt;HydrostaticCompressionTabular.uda&lt;/filebase&gt;
    &lt;outputTimestepInterval&gt;1&lt;/outputTimestepInterval&gt;
    &lt;save label = &quot;g.acceleration&quot;/&gt;
    &lt;checkpoint cycle = &quot;2&quot; timestepInterval = &quot;10&quot;/&gt;
  &lt;/DataArchiver&gt;
  &lt;MPM&gt;
    &lt;time_integrator&gt; explicit  &lt;/time_integrator&gt;
    &lt;interpolator&gt;    linear    &lt;/interpolator&gt;
  &lt;/MPM&gt;
  &lt;MaterialProperties&gt;
    &lt;MPM&gt;
      &lt;material name=&quot;TabularEOS&quot;&gt;
        &lt;density&gt;1050&lt;/density&gt;
        &lt;constitutive_model type=&quot;tabular_eos&quot;&gt;
          &lt;filename&gt; tabular_eos.json &lt;/filename&gt;
        &lt;/constitutive_model&gt;
        &lt;geom_object&gt;
          &lt;box label = &quot;Plate1&quot;&gt;
            &lt;min&gt;[0.0,0.0,0.0]&lt;/min&gt;
            &lt;max&gt;[1.0,1.0,1.0]&lt;/max&gt;
          &lt;/box&gt;
          &lt;res&gt;[1,1,1]&lt;/res&gt;
          &lt;velocity&gt;[0.0,0.0,0.0]&lt;/velocity&gt;
        &lt;/geom_object&gt;
      &lt;/material&gt;
      &lt;contact&gt;
        &lt;type&gt;null&lt;/type&gt;
        &lt;materials&gt;[0]&lt;/materials&gt;
        &lt;mu&gt;0.1&lt;/mu&gt;
      &lt;/contact&gt;
    &lt;/MPM&gt;
  &lt;/MaterialProperties&gt;
&lt;/Uintah_specification&gt;
                </code> </pre>
              </div>
            </p>
        </section>

        <section data-state="task_decl">
          <h3> Task declaration </h3>
           <div class="highlight">
             <pre> <code class="cpp" data-trim data-noescape>  
void
SerialMPM::scheduleInitializeStressAndDefGradFromBodyForce(const LevelP& level,
                                                           SchedulerP& sched)
{
  const PatchSet* patches = level->eachPatch();
  printSchedule(patches, cout_doing, "MPM::initializeStressAndDefGradFromBodyForce");

  // First compute the body force
  Task* t1 = scinew Task("MPM::initializeBodyForce",
                         this, &SerialMPM::initializeBodyForce);
  t1->requires(Task::NewDW, lb->pXLabel, Ghost::None);
  t1->modifies(lb->pBodyForceAccLabel);
  sched->addTask(t1, patches, d_sharedState->allMPMMaterials());

  // Compute the stress and deformation gradient only for selected
  // constitutive models that have a "initializeWithBodyForce" flag as true.
  // This is because a more general implementation is quite involved and
  // not worth the effort at this time. BB
  Task* t2 = scinew Task("MPM::initializeStressAndDefGradFromBodyForce",
                         this, &SerialMPM::initializeStressAndDefGradFromBodyForce);

  t2->requires(Task::NewDW, lb->pXLabel, Ghost::None);
  t2->requires(Task::NewDW, lb->pBodyForceAccLabel, Ghost::None);
  t2->modifies(lb->pStressLabel);
  t2->computes(lb->pDefGradLabel);
  sched->addTask(t2, patches, d_sharedState->allMPMMaterials());
} 
            </code> </pre>
          </div>
        </section>

        <section data-state="task_impl">
          <h3> Task implementation </h3>
            <div class="highlight">
              <pre> <code class="cpp" data-trim data-noescape>  
void
SerialMPM::initializeStressAndDefGradFromBodyForce(const ProcessorGroup* ,
                                                   const PatchSubset* patches,
                                                   const MaterialSubset* matls,
                                                   DataWarehouse*,
                                                   DataWarehouse* new_dw)
{
  // Loop over patches
  for (int p = 0; p < patches->size(); p++) {
    const Patch* patch = patches->get(p);

    printTask(patches, patch, cout_doing,
              "Doing initializeStressAndDefGradFromBodyForce");

    // Loop over materials 
    for (int m = 0; m < matls->size(); m++) {
      MPMMaterial* mpm_matl = d_sharedState->getMPMMaterial( m );

      // Compute the stress and deformation gradient only for selected
      // constitutive models that have a "initializeWithBodyForce" flag as true.
      ConstitutiveModel* cm = mpm_matl->getConstitutiveModel();
      cm->initializeStressAndDefGradFromBodyForce(patch, mpm_matl, new_dw);

    } // end matl loop

  } // end patches loop
}
              </code> </pre>
            </div>
        </section>
        
        <section data-state="component_impl">
          <h3> Application component implementation </h3>
            <div class="highlight">
              <pre> <code class="cpp" data-trim data-noescape>  
ProblemSpecP ups = VaangoEnv::createInput();
ups->getNode()->_private = (void *) ups_file.c_str();

const ProcessorGroup* world = Uintah::Parallel::getRootProcessorGroup();
SimulationController* ctl = scinew AMRSimulationController(world, false, ups);

RegridderCommon* reg = 0;
SolverInterface* solve = SolverFactory::create(ups, world, "");

UintahParallelComponent* comp = ComponentFactory::create(ups, world, false, "");
SimulationInterface* sim = dynamic_cast<SimulationInterface*>(comp);
ctl->attachPort("sim", sim);
comp->attachPort("solver", solve);
comp->attachPort("regridder", reg);

LoadBalancerCommon* lbc = LoadBalancerFactory::create(ups, world);
lbc->attachPort("sim", sim);

DataArchiver* dataarchiver = scinew DataArchiver(world, -1);
Output* output = dataarchiver;
ctl->attachPort("output", dataarchiver);
dataarchiver->attachPort("load balancer", lbc);
comp->attachPort("output", dataarchiver);
dataarchiver->attachPort("sim", sim);

SchedulerCommon* sched = SchedulerFactory::create(ups, world, output);
sched->attachPort("load balancer", lbc);
ctl->attachPort("scheduler", sched);
lbc->attachPort("scheduler", sched);
comp->attachPort("scheduler", sched);
sched->setStartAddr( start_addr );
sched->addReference();

ctl->run();
              </code> </pre>
            </div>
        </section>

        <section data-state="runtime-view">
            <h3> AMT runtime developer's view </h3>
            <p>
              <div id="box_left" style="border:1px solid white; width: 50%; float: left; font-size: 30px ">
                <ul>
                  <li> Set up data archivers to take advantage of machine specs</li>
                  <li> Create actual tasks and compile into a task graph (directed acyclic graph).
                       For tasks that need iterative actions (Newton's method), special subgraphs 
                       are created </li>
                  <li> Generate MPI calls/threads automatically based on the task graph and
                       machine architecture </li>
                  <li> Schedule tasks and perform automated load balancing </li>
                </ul>
              </div>
              <div id="box_right" style="border:1px solid white; width: 49%; float: right;">
                <img src="assets/figures/amt_developer_view.svg"/>
              </div>
              <div style="width: 50%; font-size: 16px">
                For improvements to the runtime, see <span class="blue-text">D. Sahasrabudhe (2021)
                Enhancing asynchronous many-task runtime systems for next-generation architectures
                and exascale supercomputers</span>
              </div>
            <p>
        </section>

        <!-- Material models -->

        <section data-state="material_model">
            <h3> Phenomenological plasticity </h3>
            <p>
              <div>
              <div id="box_left" style="border:1px solid white; width: 49%; float: left; background: white">
                <img src="assets/figures/ClosestPointsAndNormals3D.svg" height="360"/> 
              </div>
              <div id="box_right" style="border:1px solid white; width: 49%; float: right; background: white">
                <img src="assets/figures/ClosestPointsAndNormals_PQ_2000.svg" height="360"/>
              </div>
              </div>

              <div style="width: 100%; font-size: 30px;">
                https://bbanerjee.github.io/ParSim/assets/tech_reports/TabularInterp_Nov_2020.pdf
                https://bbanerjee.github.io/ParSim/assets/tech_reports/TabularModel_Dec_2020.pdf
              </div>
            </p>
        </section>

        <section data-state="mat-models-impl">
            <h3> Implementation of a model </h3>

            <div class="highlight">
              <pre> <code class="cpp" data-trim data-noescape>  
                d_stable = StabilityCheckFactory::create(ps);
                d_flow = FlowStressModelFactory::create(ps);
                d_damage = DamageModelFactory::create(ps);
                d_eos = MPMEquationOfStateFactory::create(ps);
                d_shear = Vaango::ShearModulusModelFactory::create(ps);
                d_elastic = std::make_unique<ElasticModuli_MetalIso>(d_eos, d_shear);
                d_melt = MeltingTempModelFactory::create(ps);
                d_devStress = DevStressModelFactory::create(ps);
                d_intvar = std::make_unique<Vaango::IntVar_Metal>(intvar_ps);
                d_yield = Vaango::YieldConditionFactory::create(ps, d_intvar.get(),
                                                                const_cast<const FlowStressModel*>(d_flow));
                ...

                void ElasticPlasticHP::computeStressTensor(const PatchSubset* patches,
                                                           const MPMMaterial* matl,
                                                           DataWarehouse* old_dw,
                                                           DataWarehouse* new_dw) {
                  // Loop thru patches
                  for (int patchIndex = 0; patchIndex < patches->size(); patchIndex++) {
                    const Patch* patch = patches->get(patchIndex);
                    auto interpolator = flag->d_interpolator->clone(patch);
                   
                    int dwi = matl->getDWIndex();
                    ParticleSubset* pset = old_dw->getParticleSubset(dwi, patch);

                    constParticleVariable<Matrix3> pDefGrad;
                    old_dw->get(pDefGrad, lb->pDefGradLabel, pset);
                    ....
                    ParticleVariable<Matrix3> pRotation_new;
                    new_dw->allocateAndPut(pRotation_new, pRotationLabel_preReloc, pset);
                    ....
                    // Loop thru particles
                    for (auto idx : *pset) {
                      ....
                      // Calculate rate of deformation tensor (D)
                      tensorD = (tensorL[idx] + tensorL[idx].Transpose()) * 0.5;

                      // Compute polar decomposition of F (F = RU)
                      pDefGrad[idx].polarDecompositionRMB(tensorU, tensorR);
                      ....

                      // Calculate flow stress
                      double flowStress = d_flow->computeFlowStress(&state_trial, delT, d_tol, matl, idx);
                      ...

                      new_dw->put(delt_vartype(delT_new), lb->delTLabel, patch->getLevel());
                    }
                  }
              </code> </pre>
             </div>

        </section>

        <!-- Gui for inputs + outputs -->
        <section data-markdown data-state="input-gui">
          <textarea data-template>
            <h3> GUI for inputs </h3>
            <p>
              <div>
              <div id="box_left" style="border:1px solid white; width: 59%; float:left; font-size: 30px">
              <ul>
                <li> For complex inputs, a separate set of tools is needed to create the input file </li>
                <li> Work on a GUI has been in progress for a while, but the complexity of 
                      pickable graphics, geometric design, and three-dimensional graphics has been
                      an issue </li>
                <li> Current development is based on ImGui, glfw + OpenGl3, and Opencascade + VTK </li>
              </ul>
              </div>
              <div id="box_right" style="border:1px solid white; width: 39%; float: right;">
                <img src="assets/figures/imgui_glfw_occt.png"/>
              </div>
              </div>
            </p>
          </textarea>
        </section>

        <section data-state="output-gui">
            <h3> Visualizing results </h3>
            <p>
              <div>
                <div style="border:1px solid white; width: 49%; float:left;">
                  <img src="assets/figures/UXO_MLP_vel22.png" height="450"/>
                </div>
                <div style="border:1px solid white; width: 49%; float: right;">
                  <img src="assets/figures/mpm_explicit_joints.png" height="450"/>
                </div>
              </div>
              <div>
                Results can be viewed using the Uintah plugin for VisIt
                <span class="blue-text">https://visit-dav.github.io/visit-website/index.html</span>
              </div>
            </p>
        </section>

        <!-- Remarks -->
        <section data-markdown data-state="remarks">
          <textarea data-template>
            ### Remarks
            
            * The material point method has matured into a capable tool
            * However, Vaango still does not have the capability of handling unstructured
              tetrahedral grids or to use GPUs efficiently
            * A lot of work is needed to update Vaango and make it easier to use
        
            <div style="font-size: 70px;">
 
               Questions?
 
            </div>
          </textarea>
        </section>
          
      </div>

    </div>

    <script src="assets/js/head.min.js"></script>
    <script src="assets/reveal.js/reveal.js"></script>
    <script src="assets/reveal.js/plugin/math/math.js"></script>
    <script src="assets/reveal.js/plugin/markdown/markdown.js"></script>
    <script src="assets/reveal.js/plugin/highlight/highlight.js"></script>
    <script src="assets/reveal.js/plugin/zoom/zoom.js"></script>
    <script src="assets/js/d3.v4.min.js"></script>
    <script src="assets/js/colorbrewer.min.js"></script>
    <script src="assets/js/particleScatter.js"></script>
    <script src="assets/js/particleScatterGhost.js"></script>
    <script src="assets/js/particleExchange.js"></script>
    <script src="assets/js/seedrandom.min.js"></script>
    <script src="assets/js/particleMigrate.js"></script>
    <script src="assets/js/particleMotion.js"></script>
    <script src="assets/js/particlePlimpton.js"></script>
    <script src="assets/js/mpmSimulation.js"></script>

    <script>

      // Full list of configuration options available at:
      // https://github.com/hakimel/reveal.js#configuration
      Reveal.initialize({
        width:1300,
        controls: true,
        controlsTutorial: false,
        progress: true,
        history: true,
        center: false,
        slideNumber: 'c/t',

        //transition: 'linear', // none/fade/slide/convex/concave/zoom
        transition: 'convex', // none/fade/slide/convex/concave/zoom
       
        math: {
          mathjax : 'assets/MathJax/MathJax.js',
          config: 'TeX-AMS_HTML-full'
        },

        plugins: [ RevealMath, RevealMarkdown, RevealHighlight, RevealZoom ],

        // Optional reveal.js plugins
        dependencies: [
          { src: 'assets/js/classList.js' },
          { src: 'assets/js/classList.js', condition: function() { return !document.body.classList; } },
          //{ src: 'assets/reveal.js/plugin/markdown/marked.js', condition: function() { return !!document.querySelector( '[data-markdown]' ); } },
          { src: 'assets/reveal.js/plugin/markdown/markdown.js', condition: function() { return !!document.querySelector( '[data-markdown]' ); } },
          { src: 'assets/reveal.js/plugin/highlight/highlight.js', async: true, condition: function() { return !!document.querySelector( 'pre code' ); }, callback: function() { hljs.initHighlightingOnLoad(); } },
          { src: 'assets/reveal.js/plugin/zoom/zoom.js', async: true },
          { src: 'assets/reveal.js/plugin/notes/notes.js', async: true }
        ]
      });

    </script>
    <script type="text/x-mathjax-config">
      MathJax.Hub.Config({ TeX: { extensions: ["color.js"] }});
    </script>


  </body>
</html>
