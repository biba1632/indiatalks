  var mpmSimulation = (function () {

    var stepCounter = 0;

    var canvas = document.querySelector("#mpm-simulation");

    var context = canvas.getContext("2d");
    var width = canvas.width;
    var height = canvas.height;

    /*
    var hiddenCanvas = d3.select("#container")
      .append('canvas')
      .classed('hiddenCanvas', true)
      .attr('width', width)
      .attr('height', height);
    */

    var xscale;
    var yscale;

    var line = d3.line()
                 .x(function(d) {return xscale(d.x);})
                 .y(function(d) {return yscale(d.y);});

    var domain = [{x: -0.0, y: -0.0},
                  {x: 1.0, y: -0.0},
                  {x: 1.0, y: 1.0},
                  {x: -0.0, y: 1.0}];

    var body1 = [ {x: 0.078802, y: 0.16009},
                  {x: 0.184793, y: 0.10167},
                  {x: 0.327650, y: 0.11335},
                  {x: 0.456682, y: 0.27111},
                  {x: 0.535023, y: 0.53696},
                  {x: 0.387558, y: 0.74730},
                  {x: 0.168664, y: 0.75606},
                  {x: 0.219355, y: 0.45516},
                  {x: 0.062673, y: 0.42010},
                  {x: 0.062673, y: 0.29740}];

    var body2 = [ {x: 0.55355, y: 0.80511},
                  {x: 0.53097, y: 0.66401},
                  {x: 0.57774, y: 0.54131},
                  {x: 0.71323, y: 0.44724},
                  {x: 0.74387, y: 0.62106},
                  {x: 0.70032, y: 0.77239},
                  {x: 0.57774, y: 0.81329}];

    var body1Pts = [];
    var body2Pts = [];

    var xgrid = [];
    var ygrid = [];
    var grid = [];

    var particles = [];

    var delT = 0.1;

    // Set up schematic
    function initialize() {

      initScales();
      initBodies();
      initDomain();
      createGrid(7, 7);
      createParticles();
      createGridForces();
      updateParticleVelocities();
      moveParticles(delT);

    }

    function draw() {

      // Set up the expanded patches first
      context.clearRect(0, 0, width, height);
      drawBodies();
      drawDomain();
      drawGrid();
      drawParticles();
      drawInternalForces();
      drawGridInternalForces();
      drawGridVelocities();
      drawGridVelocitiesNew();
      drawParticleVelocitiesNew();
      drawMovedParticles();
      drawDeformedBodies();
    }

    function initScales() {
      xscale = d3.scaleLinear()
                   .domain([-0.005, 1.005])
                   .range([0, width]);
      yscale = d3.scaleLinear()
                   .domain([-0.005, 1.005])
                   .range([height, 0]);
    }

    function initBodies() {
      for (i = 0; i < body1.length; i++) {
        body1[i].x = xscale(body1[i].x);
        body1[i].y = yscale(body1[i].y);
        body1Pts[i] = [body1[i].x, body1[i].y];
      }
      for (i = 0; i < body2.length; i++) {
        body2[i].x = xscale(body2[i].x);
        body2[i].y = yscale(body2[i].y);
        body2Pts[i] = [body2[i].x, body2[i].y];
      }
      //console.log(body1Pts);
    }

    function initDomain() {
      for (i = 0; i < domain.length; i++) {
        domain[i].x = xscale(domain[i].x);
        domain[i].y = yscale(domain[i].y);
      }
    }

    function linspace(a, b, n) {
      if (typeof n === "undefined") n = Math.max(Math.round(b-a)+1,1);
      if (n < 2) { return n===1?[a]:[]; }
      var i,ret = Array(n);
      n--;
      for (i = n; i >= 0; i--) { ret[i] = (i*b+(n-i)*a)/n; }
      return ret;
    }    
 
    function createGrid(nx, ny) {
      let seed = Math.seedrandom("84217");
      xgrid = linspace(0.0, 1.0, nx); 
      ygrid = linspace(0.0, 1.0, ny); 
      for (i = 0; i < xgrid.length; i++) {
        xgrid[i] = xscale(xgrid[i]);
      }
      for (i = 0; i < ygrid.length; i++) {
        ygrid[i] = yscale(ygrid[i]);
      }
      for (i = 0; i < xgrid.length; i++) {
        for (j = 0; j < ygrid.length; j++) {
          val = Math.random();
          let xVel = xscale((1 - val)*(-1.0) + val*1.0);
          val = Math.random();
          let yVel = yscale((1 - val)*(-1.0) + val*1.0);
          grid.push({i: i, j: j, x: xgrid[i], y:ygrid[j], xforce: 0.0, yforce: 0.0, xvel: xVel, yvel: yVel,
                     xvelnew: 0.0, yvelnew: 0.0, particles: 0});
        }
      }
    }

    // Create particles
    function createParticles() {
      
      let seed = Math.seedrandom("76543210");
      let count = 0;
      for (i = 0; i < xgrid.length-1; i++) {
        x1 = 2.0/3.0*xgrid[i] + 1.0/3.0*xgrid[i+1];
        x2 = 1.0/3.0*xgrid[i] + 2.0/3.0*xgrid[i+1];
        let xloc = [x1, x1, x2, x2];
        for (j = 0; j < ygrid.length-1; j++) {
          y1 = 2.0/3.0*ygrid[j] + 1.0/3.0*ygrid[j+1];
          y2 = 1.0/3.0*ygrid[j] + 2.0/3.0*ygrid[j+1];
          let yloc = [y1, y2, y1, y2];
          
          for (k = 0; k < 4; k++) {
            val = Math.random();
            let xForce = xscale((1 - val)*(-1.0) + val*1.0);
            val = Math.random();
            let yForce = yscale((1 - val)*(-1.0) + val*1.0);
            if (d3.polygonContains(body1Pts, [xloc[k], yloc[k]])) {
              particles.push({x: xloc[k], y: yloc[k], moved: false, id: count, xvel: 0.0, yvel: 0.0, xforce: xForce, yforce: yForce, cell: [i,j], body: 1});
              count++;
            }
            if (d3.polygonContains(body2Pts, [xloc[k], yloc[k]])) {
              particles.push({x: xloc[k], y: yloc[k], moved: false, id: count, xvel: 0.0, yvel: 0.0, xforce: xForce, yforce: yForce, cell: [i,j], body: 2});
              count++;
            }
          }
        }
      }
    }

    // Create interpolated internal forces
    function createGridForces() {
      
      for (let part of particles) {
        node1 = [part.cell[0], part.cell[1]];
        node2 = [part.cell[0]+1, part.cell[1]];
        node3 = [part.cell[0]+1, part.cell[1]+1];
        node4 = [part.cell[0], part.cell[1]+1];
        for (let node of grid) {
          if (node.i == node1[0] || node.i == node2[0] || node.i == node3[0] || node.i == node4[0]) {
            if (node.j == node1[1] || node.j == node2[1] || node.j == node3[1] || node.j == node4[1]) {
              node.xforce += part.xforce;
              node.yforce += part.yforce;
              node.particles += 1;
            }
          }
        }
      }
      for (let node of grid) {
        if (node.particles > 0) {
          node.xforce /= node.particles;
          node.yforce /= node.particles;
          node.xvelnew = (node.xvel + node.xforce)/2.0;
          node.yvelnew = (node.yvel + node.yforce)/2.0;
        }
      }
    }

    // Compute particle velocities
    function updateParticleVelocities() {
      for (let part of particles) {
        node1 = [part.cell[0], part.cell[1]];
        node2 = [part.cell[0]+1, part.cell[1]];
        node3 = [part.cell[0]+1, part.cell[1]+1];
        node4 = [part.cell[0], part.cell[1]+1];
        for (let node of grid) {
          if (node.i == node1[0] && node.j == node1[1]) {
            part.xvel += node.xvelnew;
            part.yvel += node.yvelnew;
          }
          if (node.i == node2[0] && node.j == node2[1]) {
            part.xvel += node.xvelnew;
            part.yvel += node.yvelnew;
          }
          if (node.i == node3[0] && node.j == node3[1]) {
            part.xvel += node.xvelnew;
            part.yvel += node.yvelnew;
          }
          if (node.i == node4[0] && node.j == node4[1]) {
            part.xvel += node.xvelnew;
            part.yvel += node.yvelnew;
          }
        }
        part.xvel /= 4;
        part.yvel /= 4;
      }
    }
        
    // Move particles to new position
    var moveParticles = (tt) => {
      for (let part of particles) {
        part.xnew = part.x + part.xvel*tt;
        part.ynew = part.y + part.yvel*tt;
        //console.log(part.x, part.y, part.xnew, part.ynew, part.xvel, part.yvel, tt)
      }
    }

    // Draw bodies
    function drawBezierPath(points, color) {
      context.strokeStyle = color;
      context.lineWidth = 5;
      context.beginPath();
      context.moveTo(points[0].x, points[0].y);
      for (i = 1; i < points.length-2; i++) {
        var xc = (points[i].x + points[i+1].x) / 2;
        var yc = (points[i].y + points[i+1].y) / 2;
        context.quadraticCurveTo(points[i].x, points[i].y, xc, yc);
        //console.log("xc = ", xc, "yc = ", yc);
      }
      var xc = (points[i].x + points[0].x) / 2;
      var yc = (points[i].y + points[0].y) / 2;
      context.quadraticCurveTo(points[i].x, points[i].y, xc, yc);
      context.quadraticCurveTo(xc, yc, points[0].x,points[0].y);
      context.stroke();
      context.lineWidth = 1;
    }

    function drawBodies() {
      drawBezierPath(body1, "#aa0902");
      drawBezierPath(body2, "#100902");
    }

    // Drwa bounding box
    function drawDomain() {
      context.beginPath();
      context.moveTo(domain[0].x, domain[0].y);
      context.lineTo(domain[1].x, domain[1].y);
      context.lineTo(domain[2].x, domain[2].y);
      context.lineTo(domain[3].x, domain[3].y);
      context.lineTo(domain[0].x, domain[0].y);
      context.stroke();
    }

    // Draw grid
    function drawLine(x0, y0, x1, y1) {
      context.beginPath();
      context.moveTo(x0, y0);
      context.lineTo(x1, y1);
      context.stroke();
    }

    function drawGrid() {
      for (i = 1; i < xgrid.length-1; i++) {
        let x0 = xgrid[i];
        drawLine(x0, xscale(0.0), x0, xscale(1.0));
      }
      for (i = 1; i < ygrid.length-1; i++) {
        let y0 = ygrid[i];
        drawLine(yscale(0.0), y0, yscale(1.0), y0);
      }
    }

    // Draw particles
    function drawParticles() {

      for (let part of particles) {
        //console.log([part.x, part.y]);
        context.beginPath()
        context.arc(part.x, part.y, 5, 0, 2.0*Math.PI); 
        if (part.body == 1) {
          context.fillStyle = "#aa0902";
        } else {
          context.fillStyle = "#220902";
        }
        //context.arc(xscale(part.x), yscale(part.y), 5, 0, 2.0*Math.PI); 
        //context.fillStyle = patchColors[part.patch];
        context.fill();
        context.stroke();
      }
    }

    // Draw internal force vectors
    function transform(xy, angle, xy0) {
      // put x and y relative to x0 and y0 so we can rotate around that
      const rel_x = xy[0] - xy0[0];
      const rel_y = xy[1] - xy0[1];

      // compute rotated relative points
      const new_rel_x = Math.cos(angle) * rel_x - Math.sin(angle) * rel_y;
      const new_rel_y = Math.sin(angle) * rel_x + Math.cos(angle) * rel_y;

      return [xy0[0] + new_rel_x, xy0[1] + new_rel_y];
    }

    function draw_arrow(x0, y0, x1, y1, width, head_width, head_length) {
      // compute length first
      const length = Math.sqrt((x1-x0)*(x1-x0)+(y1-y0)*(y1-y0))
      let angle  = Math.atan2(y1-y0, x1-x0);
      // adjust the angle by 90 degrees since the arrow we rotate is rotated by 90 degrees
      angle -= Math.PI / 2;

      let p0 = [x0,y0];

      // order will be: p1 -> p3 -> p5 -> p7 -> p6 -> p4 -> p2
      // formulate the two base points
      let p1 = [x0 + width / 2, y0];
      let p2 = [x0 - width / 2, y0];

      // formulate the upper base points which connect the pointy end with the lengthy thing
      let p3 = [x0 + width / 2, y0 + length - head_length];
      let p4 = [x0 - width / 2, y0 + length - head_length];

      // formulate the outter points of the triangle
      let p5 = [x0 + head_width / 2, y0 + length - head_length];
      let p6 = [x0 - head_width / 2, y0 + length - head_length];

      // end point of the arrow
      let p7 = [x0, y0 + length];

      p1 = transform(p1,angle,p0);
      p2 = transform(p2,angle,p0);
      p3 = transform(p3,angle,p0);
      p4 = transform(p4,angle,p0);
      p5 = transform(p5,angle,p0);
      p6 = transform(p6,angle,p0)
      p7 = transform(p7,angle,p0);

      // move to start first
      context.moveTo(p1[0], p1[1]);
      //context.beginPath();
      // start drawing the lines
      context.lineTo(p3[0], p3[1]);
      context.lineTo(p5[0], p5[1]);
      context.lineTo(p7[0], p7[1]);
      context.lineTo(p6[0], p6[1]);
      context.lineTo(p4[0], p4[1]);
      context.lineTo(p2[0], p2[1]);
      context.lineTo(p1[0], p1[1]);
      context.closePath();
      context.arc(x0, y0, width/2, angle-Math.PI,angle);
      context.fill();
    }

    function drawInternalForces() {
      for (let part of particles) {
        context.beginPath()
        if (part.body == 1) {
          context.fillStyle = "#aa0902";
        } else {
          context.fillStyle = "#220902";
        }
        draw_arrow(part.x, part.y, part.x+0.05*part.xforce, part.y+0.05*part.yforce, 2, 5, 15);
        context.stroke();
      }
    }

    // Draw grid internal force vectors
    function drawGridInternalForces() {
      for (let node of grid) {
        if (node.xforce*node.xforce + node.yforce*node.yforce > 1.0e-6) {
          context.beginPath()
          context.lineStyle = "#1f51ff";
          context.fillStyle = "#1f51ff";
          draw_arrow(node.x, node.y, node.x+0.1*node.xforce, node.y+0.1*node.yforce, 2, 5, 15);
          context.stroke();
        }
      }
    }

    // Draw grid velocities
    function drawGridVelocities() {
      for (let node of grid) {
        if (node.xforce*node.xforce + node.yforce*node.yforce > 1.0e-6) {
          context.beginPath()
          context.lineStyle = "#ffc300";
          context.fillStyle = "#ffc300";
          draw_arrow(node.x, node.y, node.x+0.1*node.xvel, node.y+0.1*node.yvel, 2, 5, 15);
          context.stroke();
        }
      }
    }

    function drawGridVelocitiesNew() {
      for (let node of grid) {
        if (node.xforce*node.xforce + node.yforce*node.yforce > 1.0e-6) {
          context.beginPath()
          context.lineStyle = "#ff5733";
          context.fillStyle = "#ff5733";
          draw_arrow(node.x, node.y, node.x+0.1*node.xvelnew, node.y+0.1*node.yvelnew, 2, 5, 15);
          context.stroke();
        }
      }
    }

    // Draw particle velocities
    function drawParticleVelocitiesNew() {
      for (let part of particles) {
        context.beginPath()
        context.lineStyle = "#ff5733";
        context.fillStyle = "#ff5733";
        draw_arrow(part.x, part.y, part.x+0.1*part.xvel, part.y+0.1*part.yvel, 2, 5, 15);
        context.stroke();
      }
    }

    // Draw moved particles
    function drawMovedParticles() {

      for (let part of particles) {
        context.beginPath()
        context.arc(part.xnew, part.ynew, 5, 0, 2.0*Math.PI); 
        context.fillStyle = "#aa0902";
        //context.fillStyle = patchColors[part.patch];
        context.fill();
        context.stroke();
      }
    }

    // Draw deformed bodies
    function drawDeformedBodies() {

      var body1_new = [...body1];
      var body2_new = [...body2];

      for (i = 0; i < body1.length; ++i) {
        body1_new[i].x = 1.05*body1[i].x;
        body1_new[i].y = 1.2*body1[i].y-20;
        //console.log(body1_new[i], body1[i]);
      }
      for (i = 0; i < body2_new.length; ++i) {
        body2_new[i].x = 1.2*body2[i].x-80;
        body2_new[i].y = 1.1*body2[i].y+20;
      }
      drawBezierPath(body1_new, "#aa0902");
      drawBezierPath(body2_new, "#110902");
    }

    function reset() {
      domain = [{x: -0.0, y: -0.0},
                    {x: 1.0, y: -0.0},
                    {x: 1.0, y: 1.0},
                    {x: -0.0, y: 1.0}];

      body1 = [ {x: 0.078802, y: 0.16009},
                    {x: 0.184793, y: 0.10167},
                    {x: 0.327650, y: 0.11335},
                    {x: 0.456682, y: 0.27111},
                    {x: 0.535023, y: 0.53696},
                    {x: 0.387558, y: 0.74730},
                    {x: 0.168664, y: 0.75606},
                    {x: 0.219355, y: 0.45516},
                    {x: 0.062673, y: 0.42010},
                    {x: 0.062673, y: 0.29740}];

      body2 = [ {x: 0.55355, y: 0.80511},
                    {x: 0.53097, y: 0.66401},
                    {x: 0.57774, y: 0.54131},
                    {x: 0.71323, y: 0.44724},
                    {x: 0.74387, y: 0.62106},
                    {x: 0.70032, y: 0.77239},
                    {x: 0.57774, y: 0.81329}];

      body1Pts = [];
      body2Pts = [];

      xgrid = [];
      ygrid = [];
      grid = [];

      particles = [];
    }

    let run = function () {
      initialize();
      context.fillStyle = "#ffffff";
      context.fillRect(0, 0, width, height);
      context.fillStyle = "#000000";
      drawBodies();
      drawDomain();
      context.font = "30px Arial";
      context.fillText("Two bodies in a domain", 50, 50);
      
    }

    let restartAnimation =  function () {

        console.log("step counter", stepCounter);
        reset();
        initialize();

        switch(stepCounter) {

         case 0:
           context.clearRect(0, 0, width, height);
           context.fillStyle = "#ffffff";
           context.fillRect(0, 0, width, height);
           context.fillStyle = "#000000";

           drawBodies();
           drawDomain();
           drawGrid();
           context.fillText("Add background grid", 50, 50);
           stepCounter++;
           break;
         case 1:
           context.clearRect(0, 0, width, height);
           context.fillStyle = "#ffffff";
           context.fillRect(0, 0, width, height);
           context.fillStyle = "#000000";

           drawBodies();
           drawDomain();
           drawGrid();
           drawParticles();
           context.fillText("Discretize with particles", 50, 50);
           stepCounter++;
           break;
         case 2:
           context.clearRect(0, 0, width, height);
           context.fillStyle = "#ffffff";
           context.fillRect(0, 0, width, height);
           context.fillStyle = "#000000";

           drawBodies();
           drawDomain();
           drawGrid();
           drawParticles();
           drawInternalForces();
           context.fillText("Compute internal forces", 50, 50);
           stepCounter++;
           break;
         case 3:
           context.clearRect(0, 0, width, height);
           context.fillStyle = "#ffffff";
           context.fillRect(0, 0, width, height);
           context.fillStyle = "#000000";

           drawBodies();
           drawDomain();
           drawGrid();
           drawParticles();
           drawGridInternalForces();
           drawGridVelocities();
           context.fillText("Project forces to grid", 50, 50);
           stepCounter++;
           break;
         case 4:
           context.clearRect(0, 0, width, height);
           context.fillStyle = "#ffffff";
           context.fillRect(0, 0, width, height);
           context.fillStyle = "#000000";

           drawBodies();
           drawDomain();
           drawGrid();
           drawParticles();
           drawGridVelocities();
           drawGridVelocitiesNew();
           context.fillText("Compute grid velocities", 50, 50);
           stepCounter++;
           break;
         case 5:
           context.clearRect(0, 0, width, height);
           context.fillStyle = "#ffffff";
           context.fillRect(0, 0, width, height);
           context.fillStyle = "#000000";

           drawBodies();
           drawDomain();
           drawGrid();
           drawParticles();
           drawParticleVelocitiesNew();
           context.fillText("Interpolate velocities to particles", 50, 50);
           stepCounter++;
           break;
         case 6:
           context.clearRect(0, 0, width, height);
           context.fillStyle = "#ffffff";
           context.fillRect(0, 0, width, height);
           context.fillStyle = "#000000";

           //drawBodies();
           drawDomain();
           drawGrid();
           drawMovedParticles();
           drawDeformedBodies();
           context.fillText("Move particles", 50, 50);
           stepCounter = 0;
           break;
         default:
           stepCounter = 0;
           break;
        }
    }

    return {run, restartAnimation};
   
  })();

  mpmSimulation.run();

  


